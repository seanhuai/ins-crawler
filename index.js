var express = require('express');
var fs = require('fs');
var https = require('https');
var cheerio = require('cheerio');
//var sqlite = require('sqlite3');
//var db = new sqlite.Database('database.db');
var app = express();

var acao = '*'

app.get('/', function(req, res, next) {
  var url = req.query.url||null; 
  if(url==null){
    return res.sendStatus(400);
  }
  res.setHeader('Access-Control-Allow-Origin',acao);

  var callback = function(response){
    var body = '';
    var reslist = new Object();

    response.on('data', function(data) {
      body += data; 
    });

    response.on('end', function() {
      var $ = cheerio.load(body);
      var scstr = $('body script').eq(0).html();      
      if(scstr==null){
          return res.sendStatus(400);
      }
      var respic = scstr.match(/https:\/\/[a-zA-Z_0-9-./]*\/[a-z0-9]{3}\/[0-9_]*_n.jpg/g);
      var resvideo = scstr.match(/https:\/\/[a-zA-Z_0-9-./]*_n.mp4/g);
      
      if(resvideo!=null){
        reslist['type'] = 'video';
        reslist['media'] = [];
        for (var i = 0; i < resvideo.length; i++) {
          reslist['media'][i] = resvideo[i];
        }
      }else{
        if(respic.length>1){
          respic.shift()
        }
        reslist['type'] = 'picture';        
        reslist['media'] = [];
        for (var i = 0; i < respic.length; i++) {
          reslist['media'][i] = respic[i];
        }
      }
      var json = JSON.stringify(reslist)
      res.send(json);
    });
  }

  var reqs = https.request(url,callback);
  reqs.on('error',function(e){
      console.log(e.message);
  })
  reqs.end();
});
 
app.listen(3000);