# ins-crawler

一个 Instagram 资源工具，用于获取图片或视频信息。

## 下载

```bash
git clone https://github.com/seanhuai/ins-crawler.git
cd ins-crawler
```

使用 `git clone` 命令，复制本仓库代码至本地。

## 初始化

```bash
npm install
```

使用 `npm install` 命令，安装依赖项。

## 启动

```bash
npm start
```

使用 `npm start` 命令，启动脚本。

启动默认监听本地 3000 端口。

## 参数

### url

`url` 参数为必须值。

参数内容为需要解析的 ins 帖子页 **完整地址（需以 'https' 协议字样标记地址，并以 '/' 字样结束地址）**，形如 `https://www.instagram.com/p/BZuUj5MHVXI/`。

建议使用 ins 客户端中复制帖子地址功能获取完整地址。

如 `url` 参数没有赋值，或参数内容不符合要求，则返回 400 错误。

## 返回值

```js
{
  "type":"picture",
  "media":[
    "https://scontent-sea1-1.cdninstagram.com/t51.2885-15/e35/22159332_1247717565332919_8146236707654598656_n.jpg",
    "https://scontent-sea1-1.cdninstagram.com/t51.2885-15/e35/22069973_294119171076287_2984560941355499520_n.jpg"
  ]
}
```

`type` 值指本次获取的内容类型，类型为字符串，可选值为 `picture` 或 `video`。

`media` 值指本次获取的媒体内容，类型为数组。
